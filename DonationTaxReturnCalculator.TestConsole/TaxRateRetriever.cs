namespace DonationTaxReturnCalculator.TestConsole
{
    using System;

    public class TaxRateRetriever : ITaxRateRetriever
    {
        private readonly IDataStore _store;

        public TaxRateRetriever(IDataStore store)
        {
            _store = store;
        }

        public decimal GetTaxRate(DonationType donationType)
        {
            var baseTaxRate = _store.GetBaseTaxRate();
            switch (donationType)
            {
                case DonationType.Other:
                    return baseTaxRate;
                case DonationType.HumanRights:
                    return baseTaxRate + 5;
                case DonationType.Environmental:
                    return baseTaxRate + 3;
                default:
                    throw new ArgumentOutOfRangeException(nameof(donationType), donationType, "DonationType does not exist.");
            }
        }
    }
}
