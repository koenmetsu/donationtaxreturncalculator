﻿namespace DonationTaxReturnCalculator.TestConsole
{
    using System;

    class Program
    {
        static void Main(string[] args)
        {
            var calculator = new DonationTaxReturnCalculator(new TaxRateRetriever(new DataStore()));

            // Calculate the Donation Tax Return
            Console.WriteLine("Please Enter the amount you would like to donate:");
            var donationAmount = decimal.Parse(Console.ReadLine());
            Console.WriteLine(calculator.CalculateReturnAmount(donationAmount, DonationType.Other));
            Console.WriteLine("Press any key to exit.");
            Console.ReadLine();
        }
    }
}
