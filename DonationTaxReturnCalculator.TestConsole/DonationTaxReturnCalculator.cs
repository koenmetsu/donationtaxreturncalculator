namespace DonationTaxReturnCalculator.TestConsole
{
    using System;

    public class DonationTaxReturnCalculator
    {
        private const int ReturnAmountUpperLimit = 1000;

        private readonly ITaxRateRetriever _taxRateRetriever;

        public DonationTaxReturnCalculator(ITaxRateRetriever taxRateRetriever)
        {
            _taxRateRetriever = taxRateRetriever;
        }

        public decimal CalculateReturnAmount(decimal donationAmount, DonationType donationType)
        {
            var taxRate = _taxRateRetriever.GetTaxRate(donationType);
            var ratio = taxRate / (100 - taxRate);
            var returnAmount = donationAmount * ratio;
            return Cap(Round(returnAmount));
        }

        private static decimal Cap(decimal returnAmount)
        {
            return returnAmount > ReturnAmountUpperLimit
                ? ReturnAmountUpperLimit
                : returnAmount;
        }

        private static decimal Round(decimal returnAmount)
        {
            return Math.Round(returnAmount, 2, MidpointRounding.AwayFromZero);
        }
    }
}
