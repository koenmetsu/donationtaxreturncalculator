namespace DonationTaxReturnCalculator.TestConsole
{
    public interface ITaxRateRetriever
    {
        decimal GetTaxRate(DonationType donationType);
    }
}
