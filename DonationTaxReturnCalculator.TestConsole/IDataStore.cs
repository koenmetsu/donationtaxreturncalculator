namespace DonationTaxReturnCalculator.TestConsole
{
    public interface IDataStore
    {
        decimal GetBaseTaxRate();
    }
}
