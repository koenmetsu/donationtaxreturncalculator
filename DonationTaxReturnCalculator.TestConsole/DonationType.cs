namespace DonationTaxReturnCalculator.TestConsole
{
    public enum DonationType
    {
        Other = 0,
        HumanRights = 1,
        Environmental = 2
    }
}