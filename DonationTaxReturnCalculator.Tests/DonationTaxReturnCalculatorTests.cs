namespace DonationTaxReturnCalculator.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using TestConsole;
    using FakeItEasy;

    [TestClass]
    public class DonationTaxReturnCalculatorTests
    {
        [DataTestMethod]
        [DataRow(100, 25)]
        [DataRow(200, 50)]
        public void CalculateWith20PercentTaxRate(double donationAmountInput, double expectedReturnAmountInput)
        {
            // Limitation of attributes to only allow primitives,
            // I don't know the alternative to xUnit's MemberData in MsTest to fix this for now.
            var donationAmount = Convert.ToDecimal(donationAmountInput);
            var expectedReturnAmount = Convert.ToDecimal(expectedReturnAmountInput);

            var taxRateRetriever = A.Fake<ITaxRateRetriever>();
            A.CallTo(() => taxRateRetriever.GetTaxRate(A.Dummy<DonationType>())).Returns(20m);

            var donationTaxReturnCalculator = new DonationTaxReturnCalculator(taxRateRetriever);

            var returnAmount = donationTaxReturnCalculator.CalculateReturnAmount(donationAmount, DonationType.Other);
            Assert.AreEqual(expectedReturnAmount, returnAmount);
        }

        [DataTestMethod]
        [DataRow(50, 12.5)]
        [DataRow(25, 6.25)]
        [DataRow(12.5, 3.13)] // 3.125 without rounding, chose to implement non-default rounding because it's more intuitive.
        [DataRow(6.25, 1.56)] // 1.5625 without rounding
        public void DonationTaxReturnsAreRoundedTo2Decimals(double donationAmountInput, double expectedReturnAmountInput)
        {
            // Limitation of attributes to only allow primitives,
            // I don't know the alternative to xUnit's MemberData in MsTest to fix this for now.
            var donationAmount = Convert.ToDecimal(donationAmountInput);
            var expectedReturnAmount = Convert.ToDecimal(expectedReturnAmountInput);

            var taxRateRetriever = A.Fake<ITaxRateRetriever>();
            A.CallTo(() => taxRateRetriever.GetTaxRate(A.Dummy<DonationType>())).Returns(20m);

            var donationTaxReturnCalculator = new DonationTaxReturnCalculator(taxRateRetriever);

            var returnAmount = donationTaxReturnCalculator.CalculateReturnAmount(donationAmount, DonationType.Other);
            Assert.AreEqual(expectedReturnAmount, returnAmount);
        }

        [DataTestMethod]
        [DataRow(100, 25)]
        [DataRow(400, 100)]
        [DataRow(3999, 999.75)]
        [DataRow(4000, 1000)]
        [DataRow(4001, 1000)]
        [DataRow(5000, 1000)]
        public void DonationsAreCappedAt1000Euros(double donationAmountInput, double expectedReturnAmountInput)
        {
            // Limitation of attributes to only allow primitives,
            // I don't know the alternative to xUnit's MemberData in MsTest to fix this for now.
            var donationAmount = Convert.ToDecimal(donationAmountInput);
            var expectedReturnAmount = Convert.ToDecimal(expectedReturnAmountInput);

            var taxRateRetriever = A.Fake<ITaxRateRetriever>();
            A.CallTo(() => taxRateRetriever.GetTaxRate(A.Dummy<DonationType>())).Returns(20m);

            var donationTaxReturnCalculator = new DonationTaxReturnCalculator(taxRateRetriever);

            var returnAmount = donationTaxReturnCalculator.CalculateReturnAmount(donationAmount, DonationType.Other);
            Assert.AreEqual(expectedReturnAmount, returnAmount);

        }
    }
}
