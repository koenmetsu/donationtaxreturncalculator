using FakeItEasy;

namespace DonationTaxReturnCalculator.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TestConsole;

    [TestClass]
    public class TaxRateRetrieverTests
    {
        [TestMethod]
        public void HumanRightsDonationReturns5PercentMore()
        {
            var dataStore = A.Fake<IDataStore>();
            A.CallTo(() => dataStore.GetBaseTaxRate()).Returns(20m);

            var taxRateRetriever = new TaxRateRetriever(dataStore);

            var normalTaxRate = taxRateRetriever.GetTaxRate(DonationType.Other);

            var taxRateForHumanRights = taxRateRetriever.GetTaxRate(DonationType.HumanRights);

            var diffInTaxRates = taxRateForHumanRights - normalTaxRate;

            Assert.AreEqual(diffInTaxRates, 5);
        }

        [TestMethod]
        public void EnvironmentalDonationReturns3PercentMore()
        {
            var dataStore = A.Fake<IDataStore>();
            A.CallTo(() => dataStore.GetBaseTaxRate()).Returns(40m);

            var taxRateRetriever = new TaxRateRetriever(dataStore);

            var normalTaxRate = taxRateRetriever.GetTaxRate(DonationType.Other);

            var taxRateForHumanRights = taxRateRetriever.GetTaxRate(DonationType.Environmental);

            var diffInTaxRates = taxRateForHumanRights - normalTaxRate;

            Assert.AreEqual(diffInTaxRates, 3);
        }
    }
}
